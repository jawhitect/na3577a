
# -*- coding: utf-8 -*-
"""
Created on Fri May 17 14:41:17 2019

@author: jonat
"""
import os
from pwr_sweep_processor import pwr_sweep_processor
from matplotlib import pyplot as plt

class sweep_manager:
    def __init__(self, folder):
        self.sweeps = []
        os.chdir(folder)
        sweepfiles = os.listdir()
        for sweepfile in sweepfiles:
            p = pwr_sweep_processor(sweepfile)
            self.sweeps.append(p)
        
            