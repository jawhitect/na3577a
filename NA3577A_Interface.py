# This is a class for sending and serial communications to a 3577A network Analyzer.
from NA3577A_Commands import Functions, Units
import visa

class NA3577A_Interface:

	def __init__(self):
		self.Units = Units
		self.Functions = Functions
		self.rm = visa.ResourceManager()
		self.NA = self.rm.open_resource("GPIB::11::INSTR")

	def trace_1(self):
		print("TR1")
		return self.NA.write("TR1")

	def trace_2(self):
		print("TR2")
		return self.NA.write("TR2")

	def display_function_menu(self):
		print("DSF")
		return self.NA.write("DSF")

	def log_magnitude(self):
		print("DS7")
		return self.NA.write("DS7")

	def linear_magnitude(self):
		print("DF6")
		return self.NA.write("DF6")

	def phase(self):
		print("DF5")
		return self.NA.write("DF5")

	def polar(self):
		print("DF4")
		return self.NA.write("DF4")

	def real(self):
		print("DF3")
		return self.NA.write("DF3")

	def imaginary(self):
		print("DF2")
		return self.NA.write("DF2")

	def delay(self):
		print("DF1")
		return self.NA.write("DF1")

	def delay_aperture_menu(self):
		print("DAP")
		return self.NA.write("DAP")

	def aperture_p5_percent_of_span(self):
		print("AP1")
		return self.NA.write("AP1")

	def aperture_1_percent_of_span(self):
		print("AP2")
		return self.NA.write("AP2")

	def aperture_2_percent_of_span(self):
		print("AP3")
		return self.NA.write("AP3")

	def aperture_4_percent_of_span(self):
		print("AP4")
		return self.NA.write("AP4")

	def aperture_8_percent_of_span(self):
		print("AP5")
		return self.NA.write("AP5")

	def aperture_16_percent_of_span(self):
		print("AP6")
		return self.NA.write("AP6")

	def return_(self):
		print("Ret")
		return self.NA.write("Ret")

	def input_menu(self):
		print("INP")
		return self.NA.write("INP")

	def input_eq_r(self):
		print("INR")
		return self.NA.write("INR")

	def input_eq_a(self):
		print("INA")
		return self.NA.write("INA")

	def input_eq_b(self):
		print("INB")
		return self.NA.write("INB")

	def input_eq_a_div_r(self):
		print("IAR")
		return self.NA.write("IAR")

	def input_eq_b_div_r(self):
		print("IBR")
		return self.NA.write("IBR")

	def input_eq_d1(self):
		print("ID1")
		return self.NA.write("ID1")

	def input_eq_d2(self):
		print("ID2")
		return self.NA.write("ID2")

	def input_eq_d3(self):
		print("ID3")
		return self.NA.write("ID3")

	def input_eq_d4(self):
		print("ID4")
		return self.NA.write("ID4")

	def user_defined_input(self):
		print("UDI")
		return self.NA.write("UDI")

	def input_eq_s_11(self):
		print("I11")
		return self.NA.write("I11")

	def input_eq_s_21(self):
		print("I21")
		return self.NA.write("I21")

	def input_eq_s_22(self):
		print("I22")
		return self.NA.write("I22")

	def copy_input(self):
		print("CPI")
		return self.NA.write("CPI")

	def test_set_forward(self):
		print("TSF")
		return self.NA.write("TSF")

	def test_set_reverse(self):
		print("TSR")
		return self.NA.write("TSR")

	def scale_menu(self):
		print("SCL")
		return self.NA.write("SCL")

	def autoscale(self):
		print("ASL")
		return self.NA.write("ASL")

	def reference_level(self, arg):
		if type(arg) != str:
			raise ValueError("Argument to 'reference_level' must be a string.")
		print("REF " + arg)
		return self.NA.write("REF " + arg)

	def scale_div(self, arg):
		if type(arg) != str:
			raise ValueError("Argument to 'scale_div' must be a string.")
		print("DIV " + arg)
		return self.NA.write("DIV " + arg)

	def reference_position(self, arg):
		if type(arg) != str:
			raise ValueError("Argument to 'reference_position' must be a string.")
		print("RPS " + arg)
		return self.NA.write("RPS " + arg)

	def reference_line_off(self):
		print("RL0")
		return self.NA.write("RL0")

	def reference_line_on(self):
		print("RL1")
		return self.NA.write("RL1")

	def copy_scale(self):
		print("CPS")
		return self.NA.write("CPS")

	def phase_slope(self, arg):
		if type(arg) != str:
			raise ValueError("Argument to 'phase_slope' must be a string.")
		print("PSL " + arg)
		return self.NA.write("PSL " + arg)

	def phase_slope_off(self):
		print("PS0")
		return self.NA.write("PS0")

	def phase_slope_on(self):
		print("PS1")
		return self.NA.write("PS1")

	def poser_full_scale(self, arg):
		if type(arg) != str:
			raise ValueError("Argument to 'poser_full_scale' must be a string.")
		print("PFS " + arg)
		return self.NA.write("PFS " + arg)

	def polar_phase_ref(self, arg):
		if type(arg) != str:
			raise ValueError("Argument to 'polar_phase_ref' must be a string.")
		print("PPR " + arg)
		return self.NA.write("PPR " + arg)

	def smith_chart_off(self):
		print("GT0")
		return self.NA.write("GT0")

	def smith_chart_on(self):
		print("GT1")
		return self.NA.write("GT1")

	def marker_settings_menu(self):
		print("MKR")
		return self.NA.write("MKR")

	def marker_position(self, arg):
		if type(arg) != str:
			raise ValueError("Argument to 'marker_position' must be a string.")
		print("MRP " + arg)
		return self.NA.write("MRP " + arg)

	def marker_off(self):
		print("MR0")
		return self.NA.write("MR0")

	def marker_on(self):
		print("MR1")
		return self.NA.write("MR1")

	def zero_marker(self):
		print("ZMK")
		return self.NA.write("ZMK")

	def marker_offset_off(self):
		print("MO0")
		return self.NA.write("MO0")

	def marker_offset_on(self):
		print("MO1")
		return self.NA.write("MO1")

	def marker_offset(self, arg):
		if type(arg) != str:
			raise ValueError("Argument to 'marker_offset' must be a string.")
		print("MKO " + arg)
		return self.NA.write("MKO " + arg)

	def marker_offset_freq(self, arg):
		if type(arg) != str:
			raise ValueError("Argument to 'marker_offset_freq' must be a string.")
		print("MOF " + arg)
		return self.NA.write("MOF " + arg)

	def marker_offset_amp(self, arg):
		if type(arg) != str:
			raise ValueError("Argument to 'marker_offset_amp' must be a string.")
		print("MOA " + arg)
		return self.NA.write("MOA " + arg)

	def marker_coupling_off(self):
		print("CO0")
		return self.NA.write("CO0")

	def marker_coupling_on(self):
		print("CO1")
		return self.NA.write("CO1")

	def polar_mag_offset(self, arg):
		if type(arg) != str:
			raise ValueError("Argument to 'polar_mag_offset' must be a string.")
		print("PMO " + arg)
		return self.NA.write("PMO " + arg)

	def polar_phase_offset(self, arg):
		if type(arg) != str:
			raise ValueError("Argument to 'polar_phase_offset' must be a string.")
		print("PPO " + arg)
		return self.NA.write("PPO " + arg)

	def polar_real_offset(self, arg):
		if type(arg) != str:
			raise ValueError("Argument to 'polar_real_offset' must be a string.")
		print("PRO " + arg)
		return self.NA.write("PRO " + arg)

	def polar_imag_offset(self, arg):
		if type(arg) != str:
			raise ValueError("Argument to 'polar_imag_offset' must be a string.")
		print("PIO " + arg)
		return self.NA.write("PIO " + arg)

	def polar_marker_units_Re_Im(self, arg):
		if type(arg) != str:
			raise ValueError("Argument to 'polar_marker_units_Re_Im' must be a string.")
		print("MRI " + arg)
		return self.NA.write("MRI " + arg)

	def polar_marker_units_Mg_Ph(self, arg):
		if type(arg) != str:
			raise ValueError("Argument to 'polar_marker_units_Mg_Ph' must be a string.")
		print("MMP " + arg)
		return self.NA.write("MMP " + arg)

	def marker_to_menu(self):
		print("MKG")
		return self.NA.write("MKG")

	def mkr_to_reference_level(self):
		print("MTR")
		return self.NA.write("MTR")

	def mkr_to_start_frequency(self):
		print("MTA")
		return self.NA.write("MTA")

	def mkr_to_stop_frequency(self):
		print("MTB")
		return self.NA.write("MTB")

	def mkr_to_center_frequency(self):
		print("MTC")
		return self.NA.write("MTC")

	def mkr_offset_to_span(self):
		print("MOS")
		return self.NA.write("MOS")

	def mkr_to_max(self):
		print("MTX")
		return self.NA.write("MTX")

	def mkr_to_min(self):
		print("MTN")
		return self.NA.write("MTN")

	def marker_search_menu(self):
		print("MSM")
		return self.NA.write("MSM")

	def mkr_target_value(self, arg):
		if type(arg) != str:
			raise ValueError("Argument to 'mkr_target_value' must be a string.")
		print("MTV " + arg)
		return self.NA.write("MTV " + arg)

	def mkr_to_right_for_target(self):
		print("MRT")
		return self.NA.write("MRT")

	def mkr_to_left_for_target(self):
		print("MLT")
		return self.NA.write("MLT")

	def mkr_to_full_scale(self):
		print("MTP")
		return self.NA.write("MTP")

	def mkr_to_polar_phase_ref(self):
		print("MPF")
		return self.NA.write("MPF")

	def store_data_menu(self):
		print("STO")
		return self.NA.write("STO")

	def store_in_register_d1(self):
		print("SD1")
		return self.NA.write("SD1")

	def store_in_register_d2(self):
		print("SD2")
		return self.NA.write("SD2")

	def store_in_register_d3(self):
		print("SD3")
		return self.NA.write("SD3")

	def store_in_register_d4(self):
		print("SD4")
		return self.NA.write("SD4")

	def store_and_display(self):
		print("STD")
		return self.NA.write("STD")

	def user_defined_store(self):
		print("UDS")
		return self.NA.write("UDS")

	def store_to_d1(self):
		print("TD1")
		return self.NA.write("TD1")

	def store_to_d2(self):
		print("TD2")
		return self.NA.write("TD2")

	def store_to_d3(self):
		print("TD3")
		return self.NA.write("TD3")

	def store_to_d4(self):
		print("TD4")
		return self.NA.write("TD4")

	def measurement_calibration_menu(self):
		print("CAL")
		return self.NA.write("CAL")

	def normalize(self):
		print("NRM")
		return self.NA.write("NRM")

	def normalize_short(self):
		print("NRS")
		return self.NA.write("NRS")

	def calibrate_partial(self):
		print("CPR")
		return self.NA.write("CPR")

	def calibrate_full(self):
		print("CFL")
		return self.NA.write("CFL")

	def continue_calibration(self):
		print("CGO")
		return self.NA.write("CGO")

	def define_math_menu(self):
		print("DFN")
		return self.NA.write("DFN")

	def constant_k1_real(self):
		print("KR1")
		return self.NA.write("KR1")

	def constant_k1_imaginary(self):
		print("KI1")
		return self.NA.write("KI1")

	def constant_k2_real(self):
		print("KR2")
		return self.NA.write("KR2")

	def constant_k2_imaginary(self):
		print("KI2")
		return self.NA.write("KI2")

	def constant_k3_real(self):
		print("KR3")
		return self.NA.write("KR3")

	def constant_k3_imaginary(self):
		print("KI3")
		return self.NA.write("KI3")

	def define_function(self):
		print("DFC")
		return self.NA.write("DFC")

	def function_f1(self):
		print("UF1")
		return self.NA.write("UF1")

	def function_f2(self):
		print("UF2")
		return self.NA.write("UF2")

	def function_f3(self):
		print("UF3")
		return self.NA.write("UF3")

	def function_f4(self):
		print("UF4")
		return self.NA.write("UF4")

	def function_f5(self):
		print("UF5")
		return self.NA.write("UF5")

	def math_term_for_input_r(self):
		print("R")
		return self.NA.write("R")

	def math_term_for_input_a(self):
		print("A")
		return self.NA.write("A")

	def math_term_for_input_b(self):
		print("B")
		return self.NA.write("B")

	def math_term_for_storage_reg(self):
		print("D")
		return self.NA.write("D")

	def math_term_for_constant(self):
		print("K")
		return self.NA.write("K")

	def math_term_for_function(self):
		print("F")
		return self.NA.write("F")

	def math_left_bracket(self):
		print("(")
		return self.NA.write("(")

	def math_function_plus(self):
		print("+")
		return self.NA.write("+")

	def math_function_minus(self):
		print("-")
		return self.NA.write("-")

	def math_function_multiply(self):
		print("*")
		return self.NA.write("*")

	def math_function_divide(self):
		print("/")
		return self.NA.write("/")

	def math_right_bracket(self):
		print(")")
		return self.NA.write(")")

	def increment(self):
		print("IUP")
		return self.NA.write("IUP")

	def decrement(self):
		print("IDN")
		return self.NA.write("IDN")

	def continuous_entry_off(self):
		print("CE0")
		return self.NA.write("CE0")

	def continuous_entry_on(self):
		print("CE1")
		return self.NA.write("CE1")

	def entry_off(self):
		print("HLD")
		return self.NA.write("HLD")

	def sweep_type_menu(self):
		print("STY")
		return self.NA.write("STY")

	def linear_sweep(self):
		print("ST1")
		return self.NA.write("ST1")

	def alternate_sweep(self):
		print("ST2")
		return self.NA.write("ST2")

	def log_sweep(self):
		print("ST3")
		return self.NA.write("ST3")

	def amplitude_sweep(self):
		print("ST4")
		return self.NA.write("ST4")

	def cw(self):
		print("ST5")
		return self.NA.write("ST5")

	def sweep_direction_up(self):
		print("SUP")
		return self.NA.write("SUP")

	def sweep_direction_down(self):
		print("SDN")
		return self.NA.write("SDN")

	def sweep_mode_menu(self):
		print("SMD")
		return self.NA.write("SMD")

	def continuous(self):
		print("SM1")
		return self.NA.write("SM1")

	def single_sweep(self):
		print("SM2")
		return self.NA.write("SM2")

	def manual_sweep(self):
		print("SM3")
		return self.NA.write("SM3")

	def manual_frequency(self, arg):
		if type(arg) != str:
			raise ValueError("Argument to 'manual_frequency' must be a string.")
		print("MFR " + arg)
		return self.NA.write("MFR " + arg)

	def manual_amplitude(self, arg):
		if type(arg) != str:
			raise ValueError("Argument to 'manual_amplitude' must be a string.")
		print("MAM " + arg)
		return self.NA.write("MAM " + arg)

	def marker_to_manual(self):
		print("MTM")
		return self.NA.write("MTM")

	def sweep_time_menu(self):
		print("STM")
		return self.NA.write("STM")

	def sweep_time(self, arg):
		if type(arg) != str:
			raise ValueError("Argument to 'sweep_time' must be a string.")
		print("SWT " + arg)
		return self.NA.write("SWT " + arg)

	def step_time(self, arg):
		if type(arg) != str:
			raise ValueError("Argument to 'step_time' must be a string.")
		print("SMT " + arg)
		return self.NA.write("SMT " + arg)

	def sample_time(self, arg):
		if type(arg) != str:
			raise ValueError("Argument to 'sample_time' must be a string.")
		print("MSR " + arg)
		return self.NA.write("MSR " + arg)

	def frequency_menu(self):
		print("FRQ")
		return self.NA.write("FRQ")

	def source_frequency(self, arg):
		if type(arg) != str:
			raise ValueError("Argument to 'source_frequency' must be a string.")
		print("SFR " + arg)
		return self.NA.write("SFR " + arg)

	def start_frequency(self, arg):
		if type(arg) != str:
			raise ValueError("Argument to 'start_frequency' must be a string.")
		print("FRA " + arg)
		return self.NA.write("FRA " + arg)

	def stop_frequency(self, arg):
		if type(arg) != str:
			raise ValueError("Argument to 'stop_frequency' must be a string.")
		print("FRB " + arg)
		return self.NA.write("FRB " + arg)

	def center_frequency(self, arg):
		if type(arg) != str:
			raise ValueError("Argument to 'center_frequency' must be a string.")
		print("FRC " + arg)
		return self.NA.write("FRC " + arg)

	def frequency_span(self, arg):
		if type(arg) != str:
			raise ValueError("Argument to 'frequency_span' must be a string.")
		print("FRS " + arg)
		return self.NA.write("FRS " + arg)

	def frc_step_size(self, arg):
		if type(arg) != str:
			raise ValueError("Argument to 'frc_step_size' must be a string.")
		print("CFS " + arg)
		return self.NA.write("CFS " + arg)

	def sweep_resolution_menu(self):
		print("SRL")
		return self.NA.write("SRL")

	def freq_swp_res_51_pts_per_span(self):
		print("RS1")
		return self.NA.write("RS1")

	def freq_swp_res_101_pts_per_span(self):
		print("RS2")
		return self.NA.write("RS2")

	def freq_swp_res_201_pts_per_span(self):
		print("RS3")
		return self.NA.write("RS3")

	def freq_swp_res_401_pts_per_span(self):
		print("RS4")
		return self.NA.write("RS4")

	def full_sweep(self):
		print("FSW")
		return self.NA.write("FSW")

	def freq_step_size(self, arg):
		if type(arg) != str:
			raise ValueError("Argument to 'freq_step_size' must be a string.")
		print("FST " + arg)
		return self.NA.write("FST " + arg)

	def amplitude_menu(self):
		print("AMP")
		return self.NA.write("AMP")

	def source_amplitude(self, arg):
		if type(arg) != str:
			raise ValueError("Argument to 'source_amplitude' must be a string.")
		print("SAM " + arg)
		return self.NA.write("SAM " + arg)

	def amp_step_size(self, arg):
		if type(arg) != str:
			raise ValueError("Argument to 'amp_step_size' must be a string.")
		print("AST " + arg)
		return self.NA.write("AST " + arg)

	def clear_trip_source(self):
		print("CTS")
		return self.NA.write("CTS")

	def start_amplitude(self, arg):
		if type(arg) != str:
			raise ValueError("Argument to 'start_amplitude' must be a string.")
		print("AMA " + arg)
		return self.NA.write("AMA " + arg)

	def stop_amplitude(self, arg):
		if type(arg) != str:
			raise ValueError("Argument to 'stop_amplitude' must be a string.")
		print("AMB " + arg)
		return self.NA.write("AMB " + arg)

	def steps_sweep_menu(self):
		print("NST")
		return self.NA.write("NST")

	def number_of_steps_eq_6(self):
		print("NS1")
		return self.NA.write("NS1")

	def number_of_steps_eq_11(self):
		print("NS2")
		return self.NA.write("NS2")

	def number_of_steps_eq_21(self):
		print("NS3")
		return self.NA.write("NS3")

	def number_of_steps_eq_51(self):
		print("NS4")
		return self.NA.write("NS4")

	def number_of_steps_eq_101(self):
		print("NS5")
		return self.NA.write("NS5")

	def number_of_steps_eq_201(self):
		print("NS6")
		return self.NA.write("NS6")

	def number_of_steps_eq_401(self):
		print("NS7")
		return self.NA.write("NS7")

	def trigger_mode_menu(self):
		print("TRM")
		return self.NA.write("TRM")

	def line_trigger(self):
		print("TG2")
		return self.NA.write("TG2")

	def external_trigger(self):
		print("TG3")
		return self.NA.write("TG3")

	def immediate(self):
		print("TG4")
		return self.NA.write("TG4")

	def sweep_trigger(self):
		print("TRG")
		return self.NA.write("TRG")

	def sweep_reset(self):
		print("RST")
		return self.NA.write("RST")

	def resolution_bw_menu(self):
		print("RBW")
		return self.NA.write("RBW")

	def resolution_bw_1_hz(self):
		print("BW1")
		return self.NA.write("BW1")

	def resolution_bw_10_hz(self):
		print("BW2")
		return self.NA.write("BW2")

	def resolution_bw_100_hz(self):
		print("BW3")
		return self.NA.write("BW3")

	def resolution_bw_1_khz(self):
		print("BW4")
		return self.NA.write("BW4")

	def auto_bandwidth_off(self):
		print("AU0")
		return self.NA.write("AU0")

	def auto_bandwidth_on(self):
		print("AU1")
		return self.NA.write("AU1")

	def average_menu(self):
		print("AVE")
		return self.NA.write("AVE")

	def averaging_off(self):
		print("AV0")
		return self.NA.write("AV0")

	def average_4(self):
		print("AV1")
		return self.NA.write("AV1")

	def average_8(self):
		print("AV2")
		return self.NA.write("AV2")

	def average_16(self):
		print("AV3")
		return self.NA.write("AV3")

	def average_32(self):
		print("AV4")
		return self.NA.write("AV4")

	def average_64(self):
		print("AV5")
		return self.NA.write("AV5")

	def average_128(self):
		print("AV6")
		return self.NA.write("AV6")

	def average_256(self):
		print("AV7")
		return self.NA.write("AV7")

	def attenuation_menu(self):
		print("ATT")
		return self.NA.write("ATT")

	def attenuation_r_eq_0_db(self):
		print("AR1")
		return self.NA.write("AR1")

	def attenuation_r_eq_20_db(self):
		print("AR2")
		return self.NA.write("AR2")

	def attenuation_a_eq_0_db(self):
		print("AA1")
		return self.NA.write("AA1")

	def attenuation_a_eq_20_db(self):
		print("AA2")
		return self.NA.write("AA2")

	def attenuation_b_eq_0_db(self):
		print("AB1")
		return self.NA.write("AB1")

	def attenuation_b_eq_20_db(self):
		print("AB2")
		return self.NA.write("AB2")

	def impedance_r_eq_50_omega(self):
		print("IR1")
		return self.NA.write("IR1")

	def impedance_r_eq_1_momega(self):
		print("IR2")
		return self.NA.write("IR2")

	def impedance_a_eq_50_omega(self):
		print("IA1")
		return self.NA.write("IA1")

	def impedance_a_eq_1_momega(self):
		print("IA2")
		return self.NA.write("IA2")

	def impedance_b_eq_50_omega(self):
		print("IB1")
		return self.NA.write("IB1")

	def impedance_b_eq_1_momega(self):
		print("IB2")
		return self.NA.write("IB2")

	def clear_trip_receiver(self):
		print("CTR")
		return self.NA.write("CTR")

	def length_menu(self):
		print("LEN")
		return self.NA.write("LEN")

	def length_r(self, arg):
		if type(arg) != str:
			raise ValueError("Argument to 'length_r' must be a string.")
		print("LENR " + arg)
		return self.NA.write("LENR " + arg)

	def length_r_off(self):
		print("LR0")
		return self.NA.write("LR0")

	def length_r_on(self):
		print("LR1")
		return self.NA.write("LR1")

	def length_a(self, arg):
		if type(arg) != str:
			raise ValueError("Argument to 'length_a' must be a string.")
		print("LNA " + arg)
		return self.NA.write("LNA " + arg)

	def length_a_off(self):
		print("LA0")
		return self.NA.write("LA0")

	def length_a_on(self):
		print("LA1")
		return self.NA.write("LA1")

	def length_b(self, arg):
		if type(arg) != str:
			raise ValueError("Argument to 'length_b' must be a string.")
		print("LNB " + arg)
		return self.NA.write("LNB " + arg)

	def length_b_off(self):
		print("LB0")
		return self.NA.write("LB0")

	def length_b_on(self):
		print("LB1")
		return self.NA.write("LB1")

	def length_step_size(self, arg):
		if type(arg) != str:
			raise ValueError("Argument to 'length_step_size' must be a string.")
		print("LNS " + arg)
		return self.NA.write("LNS " + arg)

	def special_functions_menu(self):
		print("SPC")
		return self.NA.write("SPC")

	def confid_self_test_menu(self):
		print("SLF")
		return self.NA.write("SLF")

	def self_test_channel_r(self):
		print("STR")
		return self.NA.write("STR")

	def self_test_channel_a(self):
		print("STA")
		return self.NA.write("STA")

	def self_test_channel_b(self):
		print("STB")
		return self.NA.write("STB")

	def beeper_off(self):
		print("BP0")
		return self.NA.write("BP0")

	def beeper_on(self):
		print("BP1")
		return self.NA.write("BP1")

	def service_diagnostics_menu(self):
		print("SDG")
		return self.NA.write("SDG")

	def source_leveling_off(self):
		print("SL0")
		return self.NA.write("SL0")

	def source_leveling_on(self):
		print("SL1")
		return self.NA.write("SL1")

	def settling_time_off(self):
		print("SE0")
		return self.NA.write("SE0")

	def settling_time_on(self):
		print("SE1")
		return self.NA.write("SE1")

	def synthesizer_diag_off(self):
		print("SY0")
		return self.NA.write("SY0")

	def synthesizer_diag_on(self):
		print("SY1")
		return self.NA.write("SY1")

	def display_test_pattern(self):
		print("DTP")
		return self.NA.write("DTP")

	def trace_memory_test(self):
		print("TMT")
		return self.NA.write("TMT")

	def fast_processor_test(self):
		print("FPT")
		return self.NA.write("FPT")

	def io_port_test(self):
		print("PRT")
		return self.NA.write("PRT")

	def more_serve_diag_menu(self):
		print("MOR")
		return self.NA.write("MOR")

	def display_memory_test(self):
		print("DST")
		return self.NA.write("DST")

	def software_revision_message(self):
		print("SRV")
		return self.NA.write("SRV")

	def s_parameters_off(self):
		print("SP0")
		return self.NA.write("SP0")

	def s_parameters_on(self):
		print("SP1")
		return self.NA.write("SP1")

	def save_instrument_state_menu(self):
		print("SAV")
		return self.NA.write("SAV")

	def save_state_in_register_1(self):
		print("SV1")
		return self.NA.write("SV1")

	def save_state_in_register_2(self):
		print("SV2")
		return self.NA.write("SV2")

	def save_state_in_register_3(self):
		print("SV3")
		return self.NA.write("SV3")

	def save_state_in_register_4(self):
		print("SV4")
		return self.NA.write("SV4")

	def save_state_in_register_5(self):
		print("SV5")
		return self.NA.write("SV5")

	def recall_instrument_state_menu(self):
		print("RCL")
		return self.NA.write("RCL")

	def recall_old_last_state(self):
		print("RLS")
		return self.NA.write("RLS")

	def recall_register_1(self):
		print("RC1")
		return self.NA.write("RC1")

	def recall_register_2(self):
		print("RC2")
		return self.NA.write("RC2")

	def recall_register_3(self):
		print("RC3")
		return self.NA.write("RC3")

	def recall_register_4(self):
		print("RC4")
		return self.NA.write("RC4")

	def recall_register_5(self):
		print("RC5")
		return self.NA.write("RC5")

	def instrument_preset(self):
		print("IPR")
		return self.NA.write("IPR")

	def plot_menu(self):
		print("PLM")
		return self.NA.write("PLM")

	def plot_all(self):
		print("PLA")
		return self.NA.write("PLA")

	def plot_trace_1(self):
		print("PL1")
		return self.NA.write("PL1")

	def plot_trace_2(self):
		print("PL2")
		return self.NA.write("PL2")

	def plot_graticule(self):
		print("PLG")
		return self.NA.write("PLG")

	def plot_characters(self):
		print("PLC")
		return self.NA.write("PLC")

	def plot_trace_1_marker(self):
		print("PM1")
		return self.NA.write("PM1")

	def plot_trace_2_marker(self):
		print("PM2")
		return self.NA.write("PM2")

	def configure_plot_menu(self):
		print("CPT")
		return self.NA.write("CPT")

	def trace_1_linetype(self, arg):
		if type(arg) != str:
			raise ValueError("Argument to 'trace_1_linetype' must be a string.")
		print("T1L " + arg)
		return self.NA.write("T1L " + arg)

	def trace_2_linetype(self, arg):
		if type(arg) != str:
			raise ValueError("Argument to 'trace_2_linetype' must be a string.")
		print("T2L " + arg)
		return self.NA.write("T2L " + arg)

	def trace_1_pen_number(self, arg):
		if type(arg) != str:
			raise ValueError("Argument to 'trace_1_pen_number' must be a string.")
		print("T1P " + arg)
		return self.NA.write("T1P " + arg)

	def trace_2_pen_number(self, arg):
		if type(arg) != str:
			raise ValueError("Argument to 'trace_2_pen_number' must be a string.")
		print("T2P " + arg)
		return self.NA.write("T2P " + arg)

	def graticule_pen_no(self, arg):
		if type(arg) != str:
			raise ValueError("Argument to 'graticule_pen_no' must be a string.")
		print("PGP " + arg)
		return self.NA.write("PGP " + arg)

	def pen_speed_fast_max(self):
		print("PNM")
		return self.NA.write("PNM")

	def pen_speed_slow(self):
		print("PNS")
		return self.NA.write("PNS")

	def set_plot_config_to_default(self):
		print("PLD")
		return self.NA.write("PLD")

	def setting_time_entry(self):
		print("STE")
		return self.NA.write("STE")

	def dump_register_a(self):
		print("DRA")
		return self.NA.write("DRA")

	def dump_register_b(self):
		print("DRB")
		return self.NA.write("DRB")

	def dump_register_r(self):
		print("DRR")
		return self.NA.write("DRR")

	def dump_register_d1(self):
		print("DD1")
		return self.NA.write("DD1")

	def dump_register_d2(self):
		print("DD2")
		return self.NA.write("DD2")

	def dump_register_d3(self):
		print("DD3")
		return self.NA.write("DD3")

	def dump_register_d4(self):
		print("DD4")
		return self.NA.write("DD4")

	def dump_trace_1(self):
		print("DT1")
		return self.NA.write("DT1")

	def dump_trace_2(self):
		print("DT2")
		return self.NA.write("DT2")

	def dump_marker_1(self):
		print("DM1")
		return self.NA.write("DM1")

	def dump_marker_2(self):
		print("DM2")
		return self.NA.write("DM2")

	def dump_marker_1_position(self):
		print("MP1")
		return self.NA.write("MP1")

	def dump_marker_2_position(self):
		print("MP2")
		return self.NA.write("MP2")

	def dump_state_learn_mode_out(self):
		print("LMO")
		return self.NA.write("LMO")

	def dump_status(self):
		print("DMS")
		return self.NA.write("DMS")

	def dump_average_number(self):
		print("DAN")
		return self.NA.write("DAN")

	def dump_key_or_knob(self):
		print("DKY")
		return self.NA.write("DKY")

	def dump_characters(self):
		print("DCH")
		return self.NA.write("DCH")

	def dump_instrument_id(self):
		print("ID?")
		return self.NA.write("ID?")

	def load_register_a(self):
		print("LRA")
		return self.NA.write("LRA")

	def load_register_b(self):
		print("LRB")
		return self.NA.write("LRB")

	def load_register_r(self):
		print("LRR")
		return self.NA.write("LRR")

	def load_register_d1(self):
		print("LD1")
		return self.NA.write("LD1")

	def load_register_d2(self):
		print("LD2")
		return self.NA.write("LD2")

	def load_register_d3(self):
		print("LD3")
		return self.NA.write("LD3")

	def load_register_d4(self):
		print("LD4")
		return self.NA.write("LD4")

	def load_state_learn_mode_in(self):
		print("LMI")
		return self.NA.write("LMI")

	def graticule_off(self):
		print("GR0")
		return self.NA.write("GR0")

	def graticule_on(self):
		print("GR1")
		return self.NA.write("GR1")

	def characters_off(self):
		print("CH0")
		return self.NA.write("CH0")

	def characters_on(self):
		print("CH1")
		return self.NA.write("CH1")

	def annotation_off(self):
		print("AN0")
		return self.NA.write("AN0")

	def annotation_on(self):
		print("AN1")
		return self.NA.write("AN1")

	def annotation_clear(self):
		print("ANC")
		return self.NA.write("ANC")

	def menu_off(self):
		print("MN0")
		return self.NA.write("MN0")

	def menu_on(self):
		print("MN1")
		return self.NA.write("MN1")

	def menu_clear(self):
		print("MNC")
		return self.NA.write("MNC")

	def ascii_data_format(self):
		print("FM1")
		return self.NA.write("FM1")

	def _64_bit_ieee_data_format(self):
		print("FM2")
		return self.NA.write("FM2")

	def _32_bit_hp_3577a_binary(self):
		print("FM3")
		return self.NA.write("FM3")

	def bus_diagnostics_mode_off(self):
		print("BD0")
		return self.NA.write("BD0")

	def bus_diagnostics_on_fast(self):
		print("BD1")
		return self.NA.write("BD1")

	def bus_diagnostics_on_slow(self):
		print("BD2")
		return self.NA.write("BD2")

	def enter_menu_user_defined(self):
		print("ENM")
		return self.NA.write("ENM")

	def enter_annotation(self):
		print("ENA")
		return self.NA.write("ENA")

	def enter_graphics(self):
		print("ENG")
		return self.NA.write("ENG")

	def clear_keyboard_buffer(self):
		print("CKB")
		return self.NA.write("CKB")

	def take_measurement(self):
		print("TKM")
		return self.NA.write("TKM")

	def set_srq_mask(self):
		print("SQM")
		return self.NA.write("SQM")

	def error_reporting_mode_0(self):
		print("ER0")
		return self.NA.write("ER0")

	def error_reporting_mode_1(self):
		print("ER1")
		return self.NA.write("ER1")

	def error_reporting_mode_2(self):
		print("ER2")
		return self.NA.write("ER2")

	def error_reporting_mode_3(self):
		print("ER3")
		return self.NA.write("ER3")

	def send_sqr(self):
		print("SQR")
		return self.NA.write("SQR")
