from NA3577A_Interface import NA3577A_Interface
from matplotlib import pyplot as plt
import numpy as np
import time
from scipy import signal
import os
from scipy.optimize import curve_fit
import pandas as pd
import atexit

class NA3577:

    def __init__(self):
        self.NA = NA3577A_Interface()

        #Set trace 1 to linear magnitude, and trace 2 to phase
        self.NA.trace_1()
        self.NA.linear_magnitude()
        self.NA.trace_2()
        self.NA.phase()

        # display trace1
        self.NA.trace_1()

        self.settings = {}  # dictionary updated/filled by get state method

        # Dictionary of user acceptable time units and corresponding command to send network analyzer
        self.timeUnits = {'S': 'SEC',  # seconds
                          'SEC': 'SEC',  # seconds
                          'MS': 'MSC',  # miliseconds
                          'MSC': 'MSC',  # miliseconds
                          'US': 'USC',  # microseconds
                          'USC': 'USC',  # microseconds
                          'NS': 'NSC',  # nanoseconds
                          'NSC': 'NSC'}  # nanoseconds

        # Dictionary of user acceptable time units and their conversion factor to seconds
        self.timeUnitsNumeric = {'S': 1,  # seconds
                          'SEC': 1,  # seconds
                          'MS': 10**(-3),  # miliseconds
                          'MSC': 10**(-3),  # miliseconds
                          'US': 10**(-6),  # microseconds
                          'USC': 10**(-6),  # microseconds
                          'NS': 10**(-9),  # nanoseconds
                          'NSC': 10**(-9)}  # nanoseconds

        self.power_Units = {
            'dB': 'DBV',
            'dBV': 'DBV',
            'dBm': 'DBM',
            'V': 'V',
            'mV': 'MV',
            'uV': 'UV',
            'nV': 'NV'
        }

        # Dictionary of user acceptable frequency units and corresponding command to send network analyzer
        self.freqUnits = {'HZ': 1, 'KHZ': 10**3, 'MHZ': 10**6}
        self.average(0)
        self.sweep_up()

        #atexit.register(self._cleanup)

        #self.settings['sweep_direction'] = 'not defined'
        #self.direction = 'not defined'

    '''
    def _cleanup(self):
        self.set_power(-40, 'dBm')
        self.NA.single_sweep()'''

    def get_state(self, pstates = False):
        self.NA.dump_characters()
        statestr = self.NA.NA.read()
        states = statestr.split(',')

        phase = states[7]
        phase = phase.split(' ')
        phase = phase[3]
        start = states[10]
        start = start.replace('START ', '')
        start = start.replace(' ', '')
        stop = states[11]
        stop = stop.replace('STOP ', '')
        stop = stop.replace(' ', '')
        drive_amp = states[12]
        drive_amp = drive_amp.replace('AMPTD ', '')
        ref = states[0]
        ref.replace('REF LEVEL ', '')
        stop_freq = stop.replace('.000Hz', '')
        # stop frequency is in Hz with the characters 'Hz' appended, so we can count the digits to find the units.
        if len(stop_freq) in (1, 2, 3):
            freq_units = 'Hz'
        elif len(stop_freq) in (4, 5, 6):
            freq_units = 'kHz'
        elif len(stop_freq) in (7, 8, 9):
            freq_units = 'MHz'
        elif len(stop_freq) in (10, 11, 12):
            freq_units = 'GHz'
        self.settings.update({'ref': ref, 'phase': phase, 'start': start, "stop": stop, "drive_amp": drive_amp, 'freq_units': freq_units})
        if pstates:
            print(statestr)

    def nsteps(self, n):
        allowed_steps = (6, 11, 21, 51, 101, 201, 401)
        if n not in allowed_steps:
            raise ValueError(str(n) + 'not allowed. Allowed number of steps are: ' + ', '.join(allowed_steps))
        command = 'self.NA.number_of_steps_eq_'+str(n)+'()'
        eval(command)
        #self.NA.number_of_steps_eq_201()


    def plot_scan(self, start, start_units, stop, stop_units, notes = ''):
        [freqs, amp, amp_units, phase, phase_units] = \
            self.scan(start, start_units, stop, stop_units)
        freq_units = self.settings['freq_units']

        fig, ax = plt.subplots(nrows=1, ncols=2)
        ax[0].plot(freqs, amp)
        ax[0].set_xlabel("Frequency ("+freq_units+')')
        ax[0].set_ylabel('Amplitude (V)')
        ax[0].set_title("Amplitude Trace")

        ax[1].plot(freqs, phase)
        ax[1].set_xlabel("Frequency (" + freq_units + ')')
        ax[1].set_ylabel("Phase (Degrees)")
        ax[1].set_title("Phase Trace")

        plt.tight_layout()
        plt.show()
        save_input = input('Enter y to save: ')

        if save_input == 'y':
            self.save(freqs, amp, phase, phase_units, start, stop, notes=notes)
            print('Save successful.')

    '''
    def plot_scan_center_span(self, freq_span, freq_span_units, freq_center, freq_center_units):
        [freqs, trace, power_units] = \
            self.scan(freq_span, freq_span_units, freq_center, freq_center_units)
        plt.plot(freqs, trace)
        plt.xlabel('Frequency (' + freq_units + ')')
        plt.ylabel('Power (' + power_units + ')')
        plt.show()
    '''

    def save_scan(self, start, start_units, stop, stop_units, name ='', notes = ''):
        [freqs, amp, phase] = \
            self.scan(start, start_units, stop, stop_units)
        self.save(freqs, amp, phase, start, stop, name=name, notes=notes)
        return [freqs, amp, phase]


    def save(self, freqs, amp, phase, start, stop, name = '', notes  = ''):
        freq_units = self.settings['freq_units']

        header = 'notes: ' + notes +\
                 '\ndrive power: ' + self.settings['drive_amp'] +\
                 '\nSweep direction: ' + self.settings['sweep_direction'] +\
                 '\nSweep time: ' + str(self.settings['sweep_time']) +\
                 '\nNumber of Averages: ' + str(self.settings['nAve']) +\
                 '\nFrequency (' + freq_units + '), Amplitude (V), Phase (Degrees)' #column headers
        tosave = np.array([freqs, amp, phase]).transpose()
        #if not name:
        datestr, timestr = self._make_time_stamp()
            #name = '_'.join(['start', str(start), start_units, 'stop', str(stop), stop_units, timestr + '.csv'])
        fname = '_'.join([datestr, name, self.settings['sweep_direction'], str(start), freq_units, 'to', str(stop), freq_units, self.settings['drive_amp'], timestr + '.csv'])
        np.savetxt(fname, tosave, delimiter=',', header=header, comments='') # without the comments argument, '# ' is inserted before each line of the header

    def _make_time_stamp(self): #, start, start_units, stop, stop_units):
        timestrct = time.localtime()
        datestr = ''.join([str(tm) for i, tm in enumerate(timestrct) if i < 3])
        timestr = str(timestrct[3])+'h'+str(timestrct[4])+'m'+str(timestrct[5])+'s'
        return (datestr, timestr)

    '''
    def scan_center_span_save(self, freq_span, freq_span_units, freq_center, freq_center_units, name = ''):
        [freqs, trace, freq_units, power_units] = \
            self.scan_center_span(freq_span, freq_span_units, freq_center, freq_center_units)

        header = 'Frequency (' + freq_units + '), Power (' + power_units + ')'
        tosave = np.array([freqs, trace]).transpose()
        if not name:
            timestrct = time.localtime()
            timestr = '_'.join([str(tm) for i, tm in enumerate(timestrct) if i < 7])
            name ='_'.join(['Center', freq_center, freq_center_units, 'Span',
                            freq_span, 'freq_span_units', timestr + '.csv'])
        np.savetxt(name, tosave, delimiter=',', header = header)
    '''

    def save_scan_series(self, start_freq, stop_freq, freq_units):
        #ToDo
        return


    def set_power(self, pwr, units):
        self.NA.source_amplitude(str(pwr) + units)

    def set_sweep_time(self, sweep_time, sweep_time_units = 's'):
        sweep_time_units = self.processTimeUnit(sweep_time_units)
        self.NA.sweep_time(str(sweep_time) + sweep_time_units)
        #time.sleep(0.2)
        self.settings['sweep_time'] = sweep_time*self.timeUnitsNumeric[sweep_time_units]

    def single_sweep(self):
        self.NA.single_sweep()

    def average(self,nAve):
        if nAve not in ('off', 0, 4, 8, 16, 32, 64, 128, 256):
            raise ValueError(str(nAve) + ' is not available. Please use 4, 8 , 16, 64, 128 or 256 averages.')
        if nAve == 'off' or nAve == 0:
            self.NA.averaging_off()
            self.settings['nAve'] = 0
            return
        command = 'self.NA.average_' + str(nAve) + '()'
        self.settings['nAve'] = nAve
        eval(command)


    def scan(self, start, start_units, stop, stop_units):
        start_units = self.processFreqUnit(start_units)
        stop_units = self.processFreqUnit(stop_units)
        if start*self.freqUnits[start_units] > stop*self.freqUnits[stop_units]:
            raise Exception("start is greater than stop")

        self.NA.start_frequency(str(start)+start_units)
        #time.sleep(0.2)
        self.NA.stop_frequency(str(stop)+stop_units)
        time.sleep(0.2)
        self.NA.continuous()
        time.sleep(0.2)

        [amp, amp_units, phase, phase_units] = self._getProcessTrace()

        freqs = np.linspace(start, stop, len(amp))

        return [freqs, amp, phase]

    # Method scan: acquires a scan at given center frequency and span with given sweep time.
    #   Inputs:
    #       freq_span - numeric value of the span of scan
    #       freq_span_units - string; the units of the numeric value of freq_span
    #       freq_center - numeric value of the center of scan
    #       freq_center_units - string; the units of the numeric value of freq_center
    #       sweep_time - numeric value of the time of scan
    #       sweep_time_units - string; the units of the numeric value of sweep_time
    #   Returns array including elements:
    #       freqs - array of numeric frequencies scanned
    #       trace - array of numeric powers measured at each frequency
    #       freq_units - string; units of frequency array values
    #       power_units = string; units of trace power array values
    def scan_center_span(self, freq_span, freq_span_units, freq_center, freq_center_units):
        freq_span_units = self.processFreqUnit(freq_span_units)
        freq_center_units = self.processFreqUnit(freq_center_units)

        self.NA.center_frequency(str(freq_center) + freq_center_units)
        self.NA.frequency_span(str(freq_span) + freq_span_units)

        [trace, amp_units] = self._getProcessTrace()

        freqs = np.linspace(
            freq_center - freq_span * self.freqUnits[freq_span_units] / (2 * self.freqUnits[freq_center_units]),
            freq_center + freq_span * self.freqUnits[freq_span_units] / (2 * self.freqUnits[freq_center_units]),
            len(trace)
        )

        freq_units = self.settings['stop'][-3:]
        if freq_units[0].isdigit():
            freq_units = freq_units[1:2]

        return [freqs, trace, freq_units, amp_units]

    # Helper method for processing unit input from the user.
    # Input:
    #   unit - user input
    #   allowedUnits - tuple of allowed units for this type of unit. Units must be upper case.
    # Returns:
    #   Value error if unit is not a string or is not in allowed units (not case sensitive).
    #   unit string in all caps if OK to be passed to network analyzer.
    def _processUnit(self, unit, allowedUnits):
        if type(unit) != str:
            raise ValueError("Units must be strings.")
        if unit.upper() not in allowedUnits:
            allowedUnitsMsg = str(allowedUnits[0:-1]) + ' or ' + str(allowedUnits[-1])
            allowedUnitsMsg.replace('(', '')
            allowedUnitsMsg.replace(')', '')
            raise ValueError("Frequency units must be " + allowedUnitsMsg + " (not case sensitive).")
        return unit.upper()

    def _getProcessTrace(self):
        if self.settings['nAve'] == 0:
            nave = 1
        else:
            nave = self.settings['nAve']
        time.sleep(self.settings['sweep_time']*nave+1)
        self.NA.dump_characters()

        for i in range(20):
            self.NA.dump_trace_1()
            amp = self.NA.NA.read()
            amp = amp.split(',')
            self.NA.dump_trace_2()
            phase = self.NA.NA.read()
            phase = phase.split(',')
            try:
                numAmp = [float(i) for i in amp]
                numPhase = [float(i) for i in phase]
                break
            except ValueError:
                print('Waiting additional 1 second...')
                time.sleep(1)
            if i == 19:
                raise Exception("Did not read trace.")

        self.get_state()
        #amp_units = ''.join(char for char in self.settings['ref'] if char.isalpha)
        #if amp_units[0].isdigit():
        #    amp_units = amp_units[1:]
        # I think amp_units are always just volts
        # ToDo, verify that this is correct
        amp_units = 'Volts'

        phase_units = self.settings['phase']
        phase_units = ''.join(char for char in phase_units if char.isalpha())
        return [numAmp, amp_units, numPhase, phase_units]

    # Transforms resonance curve into phase space, centers the resonance circle, and transforms
    # bac to frequency space to remove background.
    # Input:
    #   f - frequency array
    #   a - amplitude array
    #   ph - phase array
    # Output:
    #   returns amplitude with background removed
    def _remove_distortion(self, f, a, ph):
        xt = a * np.cos(ph)
        yt = a * np.sin(ph)
        xt -= (np.max(xt) + np.min(xt)) / 2
        yt -= (np.max(yt) + np.min(yt)) / 2
        ap = np.sqrt(xt ** 2 + yt ** 2)
        return ap

    # Processes frequency unit input from the user.
    # Input:
    #   unit - user input
    # returns:
    #   ValueError - if unit is not a string or is not in allowed units (not case sensitive).
    #   unit - string in all caps if OK to be passed to network analyzer.
    def processFreqUnit(self, unit):
        return self._processUnit(unit, tuple(self.freqUnits.keys()))

    # Processes time unit input from the user.
    # Input:
    #   unit - user input
    # returns:
    #   ValueError - if unit is not a string or is not in allowed units (not case sensitive).
    #   unit - string in all caps if OK to be passed to network analyzer.
    def processTimeUnit(self, unit):
        allowedUnits = tuple(self.timeUnits.keys())
        unit = self._processUnit(unit, allowedUnits)
        return self.timeUnits[unit]

    def findPeaks(self, start, stop, interval, units):
        name = '_'.join(["find_peaks_search",self._make_time_stamp()])
        os.mkdir(name)
        os.chdir(name)
        search_intervals = np.arange((stop-start)/interval)
        search_intervals += start
        search_intervals *= interval
        for i in search_intervals:
            [freqs, amp, amp_units, phase, phase_units] = self.scan(i, units, i + interval, units)
            sd = np.std(amp)
            [peaks, properties] = signal.find_peaks(amp, prominence=sd*4)
            for i, peak in enumerate(peaks):
                self._recursive_zoom_fit(properties['left_bases'][i], properties['right_bases'][i], peaks[i], units)
                self.average('off')

    def lorentz(self, f, f_0, fwhm, scale):
        return scale * (1 / np.pi) * (fwhm / 2) / ((f - f_0) ** 2 + (fwhm / 2) ** 2)

    def plot_curve_fit_save(self, freqs, amps, func, params, fit_label):
        plt.plot(freqs, amps, label="Measured Resonance")
        plt.plot(freqs, func(freqs, *params), label=fit_label)
        plt.xlabel('Frequency (Mhz)')
        plt.ylabel('Amplitude^2 (V^2)')
        #center = str(np.round(popt[0], decimals=4))
        #fwhm = str(np.round(popt[1], decimals=4))
        #plt.set_title(' '.join(['Center:', center, 'Mhz; FWHM:', fwhm, 'Mhz; Power:', self.am ]))
        plt.tight_layout()
        plt.legend()

    def _recursive_zoom_fit(self, start, stop, units, depth= 1):
        print("recursive")
        #[freqs, amp, freq_units, amp_units, phase, phase_units] = self.plot_scan(start, units, stop, units, save=True, ret_vals=True)
        [freqs, amp, amp_units, phase, phase_units] = self.scan(start, units, stop, units)

        maxind = np.argmax(amp)
        f0_guess = freqs[maxind]

        #fwhm guess
        hm = min(amp) + (max(amp) - min(amp)) / 2
        dist_hm = abs(np.array(amp) - hm)
        fwhm_start = np.argmin(dist_hm[0:maxind])
        fwhm_end = np.argmin(dist_hm[maxind:]) + maxind
        fwhm_guess = freqs[fwhm_end] - freqs[fwhm_start]

        scale_guess = (max(amp) - min(amp))*(np.pi*fwhm_guess)/2
        start_params = [f0_guess, fwhm_guess, scale_guess]
        popt, pcov = curve_fit(self.lorentz, freqs, amp, start_params)
        perr = np.sqrt(np.diag(pcov))

        center = np.round(popt[0], decimals=4)
        fwhm = np.round(popt[1], decimals=4)
        print('center: ', center, 'fwhm: ', fwhm)


        if fwhm < (stop-start)*2/3 and fwhm > (stop-start)/6: #and popt[2] > (max(amp)-min(amp))/2:
            self.average(64)
            self.set_sweep_time(2)
            return self.save_scan(start, units, stop, units)
        else:
            if depth > 5:
                raise Exception("Could not find good fit.")
            else:
                try:
                    return self._recursive_zoom_fit(center-2*fwhm, center+2*fwhm, units, depth=depth+1)
                except:
                    raise Exception("Could not find good fit.")


    def power_sweep(self, com, low_freq, high_freq, freq_units, start_power, interval, drive_units, nscans, notes = ''):
        dircontents = os.listdir()
        datestr, timestr = self._make_time_stamp()
        datadir = datestr + '_power_sweeps'
        if datadir in dircontents:
            os.chdir(datadir)
        else:
            os.mkdir(datadir)
            os.chdir(datadir)

        fname = ''.join([datestr, notes, 'pswp', str(low_freq), '_', str(high_freq),freq_units, '_', str(start_power),
                         't', str(start_power+interval*nscans), drive_units, '_', timestr, '.csv'])

        #sweepdir = 'power_sweep_' + str(low_freq) + '_to_' + str(high_freq)+ '_' + freq_units + '_' + com
        #os.mkdir(sweepdir)
        #os.chdir(sweepdir)
        #[freqs, amp, amp_units, phase, phase_units] = self.scan(start_low_freq, start_high_freq)

        # find optimal number of traces to include for the fit

        # sweepsdf['freq'] = sweeps.sweeps[0].traces[0].freqs
        '''
        for sweep in sweeps.sweeps:
            for trace in sweep.traces:
                sweepsdf[sweep.relpos, trace.dir, 'amplitude', trace.power] = trace.amp
                sweepsdf[sweep.relpos, trace.dir, 'phase', trace.power] = trace.phase
                '''
        drive_powers = start_power + np.arange(nscans)*interval
        direction = ['up', 'down']
        measurement = ['amplitude', 'phase']
        names = ['Direction', 'Measurement', 'Drive (%s)' % drive_units, ]
        #drives_w_units = [str(dr)+' (%s)'%drive_units for dr in drive_powers]
        index = pd.MultiIndex.from_product([direction, measurement, drive_powers], names=names) #drives_w_units], names=names)

        #for i in range(nscans):
        for i, drive in enumerate(drive_powers):

            self.set_power(drive, drive_units)#(start_power + i * interval, power_units)
            self.sweep_up()

            #self.save_scan(low_freq, freq_units, high_freq, freq_units, com)
            freqs, amp, phase = self.scan(low_freq, freq_units, high_freq, freq_units)

            # after first scan, initialize the data frame settting its indices to the frequencies.
            if i == 0:
                freqs_ind = pd.Index(freqs, name='Frequency (%s)' % freq_units)
                sweepsdf = pd.DataFrame(index=freqs_ind, columns=index)
            sweepsdf['up', 'amplitude', drive] = amp#drives_w_units[i]] = amp
            sweepsdf['up', 'phase', drive] = phase #drives_w_units[i]] = phase

            self.sweep_down()
            freqs, amp, phase = self.scan(low_freq, freq_units, high_freq, freq_units)

            sweepsdf['down', 'amplitude', drive] = amp #drives_w_units[i]] = amp
            sweepsdf['down', 'phase', drive] = phase #drives_w_units[i]] = phase
        sweepsdf.to_csv(fname)
        os.chdir('..')
        return(fname, datadir)
        #os.chdir('..')


    def sweep_down(self):
        self.NA.sweep_direction_down()
        self.settings['sweep_direction'] = 'down'

    def sweep_up(self):
        self.NA.sweep_direction_up()
        self.settings['sweep_direction'] = 'up'












