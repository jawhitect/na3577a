# -*- coding: utf-8 -*-
"""
Created on Fri Apr 12 17:46:04 2019

@author: jonat
"""
import numpy as np
from scipy.optimize import least_squares
from matplotlib import pyplot as plt
from curve_shapes import duffing_curve, lorentz, duffing_backbone, dbackbone_cantilever_displacement_calibration

def fit_duffing(freqs, amps, direction): #,f0_start,  g_start, Q_start, eta_start):
    lorFit = fit_lorentzian(freqs, amps)
    
    #f0_guess = lorFit.x[0]
    g_guess = np.sqrt(lorFit.x[1]/((np.pi*lorFit.x[2])/2))
    f0_guess = lorFit.x[0] #- g_guess
    Q_guess = f0_guess/lorFit.x[2] #1000 
    #g_guess = #np.sqrt(lorFit.x[1]*(1/np.pi)*(f0_guess/(2*Q_guess)))#(fwhm/2))

    eta_guess = 0.1
    
    b_guess = lorFit.x[3]
    
    a_guess = 1
    g_guess = g_guess/a_guess
    
    start = [f0_guess, g_guess, Q_guess, eta_guess, a_guess, b_guess]
    print('f0 start: ', start)
    
    #bounds = ((freqs[0], freqs[-1]), (0, Q_guess), (100, np.inf), (0, np.inf), (0, 1), (0, b_guess*2))
    bounds = ((freqs[0], 0, 100, 0, 0, 0),(freqs[-1], Q_guess, np.inf, np.inf, 1, b_guess*2))
    print('bounds: ', bounds)
    def duffing_trial_residuals(x):
        residuals = amps - duffing_curve(freqs, *x, direction)
        #plt.plot(freqs, amps, label = 'data')
        #plt.plot(freqs, duffing_curve(freqs, *x, direction), label = 'trial')
        #plt.legend()
        #plt.show()
        print(x)
        print('SSE: ', np.sum(residuals**2))
        return residuals
    return least_squares(duffing_trial_residuals, start, bounds = bounds)

def fit_lorentzian(freqs, amps):
    
    maxind = np.argmax(amps)
    f0_guess = freqs[maxind]
    
    #fwhm guess
    hm = min(amps) + (max(amps) - min(amps)) / 2
    print('hm :', hm)
    dist_hm = np.abs(np.array(amps) - hm)
    #print(dist_hm)
    fwhm_start_ind = np.argmin(dist_hm[0:maxind])
    fwhm_end_ind = np.argmin(dist_hm[maxind:]) + maxind
    print('fwhm: ', freqs[fwhm_start_ind], freqs[fwhm_end_ind])
    fwhm_guess = freqs[fwhm_end_ind] - freqs[fwhm_start_ind]

    scale = (max(amps)-min(amps))*(np.pi*fwhm_guess)/2
    b_guess = min(amps)
    #scale_guess = (max(amps)-min(amps))/(1/np.pi)*(fwhm_guess/2)
    #start_params = [f0_guess, fwhm_guess, scale_guess]
    start_params = [f0_guess, scale, fwhm_guess, b_guess]
    print(start_params)
    def lorentzian_trial_residuals(params):
        return amps - lorentz(freqs, *params)
    fit = least_squares(lorentzian_trial_residuals, start_params)
    return fit

'''
fit_duffing_backbone fits the backbone to a series of duffing curves observed in response to different drive amplitudes to
give the resonant frequency and the duffing nonlinearity
'''
def fit_duffing_backbone(max_displcements, max_omegas, m_eff, omega_n_guess, alpha_est):
    #u_sq_maxes = max_displcements**2
    def duffing_backbone_residuals(params):
        omega_n = params[0]
        alpha_eff = params[1]
        return max_omegas - duffing_backbone(max_displcements, omega_n, alpha_eff, m_eff)

    start_params = [omega_n_guess, alpha_est]
    return least_squares(duffing_backbone_residuals, start_params)

'''
fit_duffing_backbone fits the backbone to a series of duffing curves observed in response to different drive amplitudes to
give the resonant frequency and the duffing nonlinearity
'''
def fit_duffing_backbone_fn(max_displacements, max_omegas, alpha_d_m_eff, f_n_guess):
    def duffing_backbone_residuals(params):
        f_n = params[0]
        alpha_d_meff = params[1]
        return max_omegas - duffing_backbone(max_displacements, f_n, alpha_d_meff, 1)

    start_params = [f_n_guess, alpha_d_m_eff]
    return least_squares(duffing_backbone_residuals, start_params,
                         bounds = (
                             (f_n_guess - f_n_guess/100, f_n_guess + f_n_guess/100),
                             (alpha_d_m_eff/2,2*alpha_d_m_eff)
                            )
                         )
'''
Calibrate the displacement of a cantilever in relation to the photodiode readout voltage.

Inputs:
    f_m - vector of frequencies where the maximum voltage of a resonance curve occurs
    v_m - vector of maximum voltages of resonance curves corresponding to f_m
    L - the length of the cantilever. This is not fit because it is not independent of alpha. Defines the units of the calibration.
    fr_start - starting guess for the unshifted resonant frequency
    alpha_start - starting guess for the duffing coefficient, calculated from expected manufacturing geometry
    calib_coef - defines the relation between photodiode voltage and cantilever displacement in the units that L is given:
                    V = calib_coef * x
    bounds - tuple of tuples giving the bounds for the fit paramenters. Should follow the form:
            bounds = ((fr_min, alpha_min, calib_coef_min, offset_min), (fr_max, alpha_max, calib_coef_max, offset_max))
    
'''
def dbackbone_calibration(f_m, v_m, L, fr_start, alpha_start, calib_coef_start, offset_start, bounds):
    def calib_residuals(params):
        f_r = params[0]
        alpha = params[1]
        calib_coef = params[2]
        offset = params[3]
        return v_m**2 - dbackbone_cantilever_displacement_calibration(f_m, L, f_r, alpha, calib_coef, offset)
    start_params = [fr_start, alpha_start, calib_coef_start, offset_start]
    return least_squares(calib_residuals, start_params, bounds = bounds)