# -*- coding: utf-8 -*-
"""
Created on Fri May  3 11:54:27 2019

@author: jonat
"""

import sympy
import numpy as np
from sympy import sqrt, I

def lorentz(f, f_0, scale, fwhm, b):
    return scale*(1/np.pi)*(fwhm/2)/((f-f_0)**2 +(fwhm/2)**2)+b
#def lorentz(f, f_0, peak_amp, fwhm, b):
    #return peak_amp/((f-f_0)**2 +(fwhm/2)**2) + b


Asq, Omega, Q, g, om, om_n, eta = sympy.symbols('Asq Omega Q g om om_n eta')
s1 = (-8*(eta - 6*Omega))/(3.*(9 + eta**2)) - (2**0.3333333333333333* (432 - 16*eta**2 + 768*eta*Omega - 576*Omega**2 + 192*eta**2*Omega**2))/(3.*(9 + eta**2)*(34992*g**2 + 10368*eta + 7776*g**2*eta**2 + 128*eta**3 + 432*g**2*eta**4 - 62208*Omega + 11520*eta**2*Omega - 69120*eta*Omega**2 + 4608*eta**3*Omega**2 - 27648*Omega**3 - 27648*eta**2*Omega**3 + sqrt(4*(432 - 16*eta**2 + 768*eta*Omega - 576*Omega**2 + 192*eta**2*Omega**2)**3 + (34992*g**2 + 10368*eta + 7776*g**2*eta**2 + 128*eta**3 + 432*g**2*eta**4 - 62208*Omega + 11520*eta**2*Omega - 69120*eta*Omega**2 + 4608*eta**3*Omega**2 - 27648*Omega**3 - 27648*eta**2*Omega**3)**2))**  0.3333333333333333) + (34992*g**2 + 10368*eta + 7776*g**2*eta**2 + 128*eta**3 + 432*g**2*eta**4 - 62208*Omega + 11520*eta**2*Omega - 69120*eta*Omega**2 + 4608*eta**3*Omega**2 - 27648*Omega**3 - 27648*eta**2*Omega**3 + sqrt(4*(432 - 16*eta**2 + 768*eta*Omega - 576*Omega**2 + 192*eta**2*Omega**2)**3 + (34992*g**2 + 10368*eta + 7776*g**2*eta**2 + 128*eta**3 + 432*g**2*eta**4 - 62208*Omega + 11520*eta**2*Omega - 69120*eta*Omega**2 + 4608*eta**3*Omega**2 - 27648*Omega**3 - 27648*eta**2*Omega**3)**2))**0.3333333333333333/(3.*2**0.3333333333333333*(9 + eta**2))
s2 = (-8*(eta - 6*Omega))/(3.*(9 + eta**2)) + ((1 + I*sqrt(3))* (432 - 16*eta**2 + 768*eta*Omega - 576*Omega**2 + 192*eta**2*Omega**2))/ (3.*2**0.6666666666666666*(9 + eta**2)* (34992*g**2 + 10368*eta + 7776*g**2*eta**2 + 128*eta**3 + 432*g**2*eta**4 - 62208*Omega + 11520*eta**2*Omega - 69120*eta*Omega**2 + 4608*eta**3*Omega**2 - 27648*Omega**3 - 27648*eta**2*Omega**3 + sqrt(4*(432 - 16*eta**2 + 768*eta*Omega - 576*Omega**2 + 192*eta**2*Omega**2)**3 + (34992*g**2 + 10368*eta + 7776*g**2*eta**2 + 128*eta**3 + 432*g**2*eta**4 - 62208*Omega + 11520*eta**2*Omega - 69120*eta*Omega**2 + 4608*eta**3*Omega**2 - 27648*Omega**3 - 27648*eta**2*Omega**3)**2))**  0.3333333333333333) - ((1 - I*sqrt(3))* (34992*g**2 + 10368*eta + 7776*g**2*eta**2 + 128*eta**3 + 432*g**2*eta**4 - 62208*Omega + 11520*eta**2*Omega - 69120*eta*Omega**2 + 4608*eta**3*Omega**2 - 27648*Omega**3 - 27648*eta**2*Omega**3 + sqrt(4*(432 - 16*eta**2 + 768*eta*Omega - 576*Omega**2 + 192*eta**2*Omega**2)**3 + (34992*g**2 + 10368*eta + 7776*g**2*eta**2 + 128*eta**3 + 432*g**2*eta**4 - 62208*Omega + 11520*eta**2*Omega - 69120*eta*Omega**2 + 4608*eta**3*Omega**2 - 27648*Omega**3 - 27648*eta**2*Omega**3)**2))**0.3333333333333333)/(6.*2**0.3333333333333333*(9 + eta**2))
s3 = (-8*(eta - 6*Omega))/(3.*(9 + eta**2)) + ((1 - I*sqrt(3))*(432 - 16*eta**2 + 768*eta*Omega - 576*Omega**2 + 192*eta**2*Omega**2))/(3.*2**0.6666666666666666*(9 + eta**2)*(34992*g**2 + 10368*eta + 7776*g**2*eta**2 + 128*eta**3 + 432*g**2*eta**4 - 62208*Omega + 11520*eta**2*Omega - 69120*eta*Omega**2 + 4608*eta**3*Omega**2 - 27648*Omega**3 - 27648*eta**2*Omega**3 + sqrt(4*(432 - 16*eta**2 + 768*eta*Omega - 576*Omega**2 + 192*eta**2*Omega**2)**3 + (34992*g**2 + 10368*eta + 7776*g**2*eta**2 + 128*eta**3 + 432*g**2*eta**4 - 62208*Omega +  11520*eta**2*Omega - 69120*eta*Omega**2 + 4608*eta**3*Omega**2 - 27648*Omega**3 - 27648*eta**2*Omega**3)**2))** 0.3333333333333333) - ((1 + I*sqrt(3))* (34992*g**2 + 10368*eta + 7776*g**2*eta**2 + 128*eta**3 + 432*g**2*eta**4 - 62208*Omega + 11520*eta**2*Omega -  69120*eta*Omega**2 + 4608*eta**3*Omega**2 - 27648*Omega**3 - 27648*eta**2*Omega**3 +  sqrt(4*(432 - 16*eta**2 + 768*eta*Omega - 576*Omega**2 + 192*eta**2*Omega**2)**3 +  (34992*g**2 + 10368*eta + 7776*g**2*eta**2 + 128*eta**3 + 432*g**2*eta**4 - 62208*Omega +  11520*eta**2*Omega - 69120*eta*Omega**2 + 4608*eta**3*Omega**2 - 27648*Omega**3 - 27648*eta**2*Omega**3)**2))**0.3333333333333333)/(6.*2**0.3333333333333333*(9 + eta**2))
dsols = (s1, s2, s3)

def duffing_curve_h(Omegas, g_in, eta_in, swpdir):
    '''
    This function calculates the shape of an observed resonance curve of
    a duffing resonator with hysteresis. 
    input:
        Omega - numpy array of drive frequencies for which to compute the duffing resonance amplitude
        g - the driving amplitude
        Q - the Q factor of the resonator
        direction - the direction of the sweep, to account for hysteresis.
                    If 'u' (upward), then the unstable solution is ignored, and 
                        the upper solution is returned where it overlaps with the lower solution,
                        and the lower solution is not
                    If 'd' (downward), then the unstable solution is ignored, and 
                        the upper solution is returned where it overlaps with the lower solution,
                        and the lower solution is not        
    returns: array of amplitude solutions for each Omega
    '''
    if swpdir == 'up':
        sols = [s1,s2]
    elif swpdir == 'down':
        sols = [s2,s1]
        Omegas = np.flip(Omegas)
    else:
        raise ValueError(str(swpdir) + ' is not a valid input for the direction input: must be "up" (upward) or "down" (downward).')
    
    # curve[0] has the driving frequencies and curve[1] has the amplitudes when driven at the corresponding frequency
    #curve = [[],[]]
    amps = []
    for s in (0,1): 
        sols[s] = sols[s].subs([(g, g_in), (eta, eta_in)])
    i = 0
    for om in Omegas:
        num = sols[i].subs(Omega, om).evalf()
        # check whether num is complex. //If the imaginary component < 10^15, assume floating point error 
        if abs(sympy.im(num)) < 10**(-10):
            amps.append(float(sympy.re(num)))
        else:
            i += 1
            num = sols[i].subs(Omega, om).evalf()
            amps.append(float(sympy.re(num)))
            
    amps = np.array(amps)
        
    if swpdir == 'down':
        amps = np.flip(amps)
    return amps

def duffing_curve(os, o_res, g_in, Q_in, eta_in, a, b, swpdir):
    '''
    This function calculates the shape of an observed resonance curve of
    a duffing resonator with hysteresis. 
    input:
        Omega - numpy array of drive frequencies for which to compute the duffing resonance amplitude
        g - the driving amplitude
        Q - the Q factor of the resonator
        direction - the direction of the sweep, to account for hysteresis.
                    If 'None', then all solutions are returned
                    If 'f' (forward), then the unstable solution is ignored, and 
                        the upper solution is returned where it overlaps with the lower solution,
                        and the lower solution is not
                    If 'b' (backward), then the unstable solution is ignored, and 
                        the upper solution is returned where it overlaps with the lower solution,
                        and the lower solution is not        
    returns: array of amplitude solutions for each Omega
    '''
    Oms = Q_in*(os/o_res-1)
    c = a*duffing_curve_h(Oms, g_in, eta_in, swpdir) + b
    #c = np.sqrt(c)
    #c = np.nan_to_num(c)
    return c

def duffing_phase(os, Asq, o_res, fwhm, m, alpha, eta):
    return

'''
The duffing backbone gives the peak frequency of a duffing curve
----
Inputs:
    u_sq - the maximum displacement of the oscillator squared
    omega_n - the resonant frequency of the mode 
    alpha_eff - the duffing nonlinearity
    m_eff - the effective mass of the oscillator
Returns: Frequency Max
'''
def duffing_backbone(u_max, omega_n, alpha, m_eff):
    return omega_n + 3*(u_max**2)*alpha/(8*m_eff*omega_n)

'''
Taken from equation 3 of Villanueva et al. Fitting this to the output voltage of the photodiode 
calibrates the displacement of the cantilever:
v_max^2 = (calib_coef^2)*(x_max^2) . x_max is in whatever units L is supplied in.
Squares are used to avoid NAN errors from taking the root of a negative number during fitting (freq_max - f_r)
'''
def dbackbone_cantilever_displacement_calibration(freq_max, L, f_r, alpha, calib_coef, offset):
    numerator = 8*(freq_max-f_r)*(L*calib_coef)**2
    denominator = 3*f_r*alpha
    return(numerator/denominator + offset)#*np.sqrt(8*(L**2)*(freq_max-f_r)/(3*f_r*alpha))