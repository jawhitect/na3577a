# This script from the commands and units dictionaries containing accepted commands and units for the
# 3577A Network Analyzer, generates a class for communicating with the Network analyzer from python

from NA3577A_Commands import Functions, Units
outfile = open('NA3577A_Interface.py','w')

outfile.write('# This is a class for sending and serial communications to a 3577A network Analyzer.\n')
outfile.write('from NA3577A_Commands import Functions, Units\n')
outfile.write('import visa\n')
#outfile.write('import pyvisa\n')
outfile.write('\n')
outfile.write('class NA3577A_Interface:\n')
outfile.write('\n')

#define constructor
outfile.write('\tdef __init__(self):\n')
outfile.write('\t\tself.Units = Units\n')
outfile.write('\t\tself.Functions = Functions\n')
outfile.write('\t\tself.rm = visa.ResourceManager()\n')
outfile.write('\t\tself.NA = self.rm.open_resource("GPIB::11::INSTR")\n')


#indicator value whether method takes input
input = 0

for name, cmd in Functions.items():
    outfile.write('\n')
    if '(entry)' in  name:
        name = name.replace(' (entry)', '')
        args = '(self, arg)' #for the method definition
        input = 1
    else:
        args = '(self)'
        input = 0
    name = name.replace(' ', '_')
    defline = '\tdef ' + name + args + ":\n"
    outfile.write(defline)
    if input:
        outfile.write('\t\tif type(arg) != str:\n')
        outfile.write('\t\t\traise ValueError(\"Argument to \'' + name + '\' must be a string.\")\n')
        toSend = cmd + ' " + arg'
        outfile.write('\t\tprint("' + toSend + ')\n')
        #outfile.write('\t\treturn self.NA.write("' + cmd + ' " + arg)\n')
        outfile.write('\t\treturn self.NA.write("' + toSend + ')\n')
    else:
        outfile.write('\t\tprint("' + cmd + '")\n')
        outfile.write('\t\treturn self.NA.write("'+cmd+'")\n')


outfile.close()