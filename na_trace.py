# -*- coding: utf-8 -*-
"""
Created on Tue Jun 25 15:44:42 2019

@author: jonat
"""

import pandas as pd
from matplotlib import pyplot as plt
import numpy as np
from fit_funcs import fit_duffing, fit_lorentzian
from curve_shapes import duffing_curve, lorentz

class trace:
    def __init__(self, file_name):
        with open(file_name) as f:
            self.note = f.readline()
            self.power = f.readline().split(':')[1].replace('\n', '').strip()
            self.dir = f.readline().split(':')[1].replace('\n', '').strip()
            self.sweep_time = f.readline().split(':')[1].replace('\n', '').strip()
            self.n_ave = f.readline().split(':')[1].replace('\n', '').strip()
    
        trace = pd.read_csv(file_name, skiprows = 5, header = 1)
        keys = trace.keys()
        #ToDo: get frequency units from keys
        self.freqs = np.array(trace[keys[0]])
        self.amp = np.array(trace[keys[1]])*1000 #convert to mV with factof 1000
        self.phase = np.array(trace[keys[2]])
        
    def plot(self):
        
        #plt.plot(self.freqs, self.amp, label = "Amplitude")
        #ph = (self.phase/np.max(self.phase))*np.max(self.freqs)/2
        #plt.plot(self.freqs, ph, label = "Phase (scaled)")
        fig, ax = plt.subplots(nrows = 1, ncols = 2)
        #ax[0].set_title("Upward Scans")
        #ax[1].set_title("Downward Scans")
        ax[0].plot(self.freqs, self.amp, label = "Amplitude")
        ax[0].set_xlabel("Frequency (kHz)")
        ax[0].set_ylabel("Amplitude (a.u.)")
        ax[1].plot(self.freqs, self.phase, label = "Phase")
        #plt.title(self.power)
        ax[1].set_xlabel('Frequency (kHz)')
        #plt.ylabel('Amplitude (mV)')
        ax[1].set_ylabel('Phase (degrees)')
        plt.legend()
        plt.show()

    def plot_duffing_fit(self):
        fit = fit_duffing(self.freqs, self.amp, self.dir)
        plt.plot(self.freqs, self.amp, color='k', label='Observed')
        plt.plot(self.freqs, duffing_curve(self.freqs, *fit.x, self.dir), color='r', label='Duffing Fit')
        plt.xlabel('Frequency (kHz)')
        plt.ylabel('Amplitude (mV)')

        