
#import os
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
#from fit_funcs import fit_duffing, fit_lorentzian
from na_trace import trace
from scipy.optimize import least_squares
from curve_shapes import duffing_backbone, lorentz, duffing_curve, dbackbone_cantilever_displacement_calibration
from fit_funcs import fit_duffing_backbone, fit_duffing_backbone_fn, dbackbone_calibration

class pwr_sweep_processor:
    def __init__(self, pwr_swp_file):#pwr_swp_dir):
        #if pwr_swp_file[-4:] == '.csv':
        self.sweep = pd.read_csv(pwr_swp_file, index_col=0, header=[0, 1, 2])

        self.unit_mags = {'kHz': 1000, 'MHz': 1000000}
        '''
        os.chdir(pwr_swp_file)
        self.name = pwr_swp_file
        self.traces = []
        files = os.listdir()
        
        for file in files:
            self.traces.append(trace(file))
            
        self.traces.sort(key=lambda trace: float(''.join([char for char in trace.power if not char.isalpha()])), reverse=True)
        os.chdir('..')
        '''
            
            
    def plot_amp_traces(self, fstart = None, fstop = None):

        fig, ax = plt.subplots(nrows = 1, ncols = 2)
        ax[0].set_title("Upward Scans")
        ax[1].set_title("Downward Scans")
        self.sweep['up','amplitude'].plot(ax = ax[0])
        self.sweep['down', 'amplitude'].plot(ax=ax[1])
        '''
        
        for trace in self.traces:
            istart, istop = self._get_indices()
                
            if trace.dir == 'up':
                ax[0].plot(trace.freqs[istart:], trace.amp, label = trace.power)
            elif trace.dir == 'down':
                ax[1].plot(trace.freqs, trace.amp, label = trace.power)
                
        for i in (0, 1):
            ax[i].set_xlabel('Frequency (khz)')
            ax[i].set_ylabel('Amplitude (mV)')
            ax[i].legend()
        plt.show()
        '''
        
    def plot(self):
        fig, ax = plt.subplots(nrows = 2, ncols = 2)
        ax[0,0].set_title("Upward Scans")
        ax[0,1].set_title("Downward Scans")
        
        for trace in self.traces:
            if trace.dir == 'up':
                ax[0,0].plot(trace.freqs, trace.amp, label = trace.power)
                ax[1,0].plot(trace.freqs, trace.phase, label = trace.power)
            elif trace.dir == 'down':
                ax[0,1].plot(trace.freqs, trace.amp, label = trace.power)
                ax[1,1].plot(trace.freqs, trace.phase, label = trace.power)
                
        for dire in (0, 1):
            for ap in (0,1):
                ax[dire, ap].set_xlabel('Frequency (khz)')
                if ap:
                    ax[ap, dire].set_ylabel('Phase (Deg)')
                else:
                    ax[ap, dire].set_ylabel('Amplitude (mV)')
                ax[ap, dire].legend()
        fig.tight_layout()
        fig.tight_layout()
        plt.show()
        
    def plot_trace_amp(self, i):
        return
        
        
    def _get_index_closest_to(self, f, trace_num):
        abs_diff = np.abs(self.traces[trace_num] - f)
        return np.argmin(abs_diff)
    
    def _get_indices(self, fstart, fstop):                
        if fstart:
            istart = self._get_index_closest_to(fstart, trace)
        else:
            istart = 0
        if fstop:
            istop = self._get_index_closest_to(fstop, trace)
        else:
            istop = len(trace.freqs)
        return((istart, istop))

    '''
    fit_duffing_backbone fits the backbone to a series of duffing curves observed in response to different drive amplitudes to
    give the resonant frequency and the duffing nonlinearity
    '''
    ''''
    def fit_duffing_backbone(self, m_eff, omega_n_guess, dir):
        u_sq_maxes = np.array([max(trace.amp) for trace in self.traces if trace.dir == dir])**2
        omega_maxes = np.array([trace.freqs[np.argmax(trace.amp)] for trace in self.traces if trace.dir == dir])
        def duffing_backbone_residuals(params):
            omega_n = params[0]
            alpha_eff = params[1]
            return omega_maxes - duffing_backbone(u_sq_maxes, omega_n, alpha_eff, m_eff)
        start_params = [omega_n_guess, 0.001]
        return least_squares(duffing_backbone_residuals, start_params)
        '''

    '''
    This method fits a duffing backbone curve to the power sweep, plots the fit, and returns the fit parameters including
    the calibration.
    
    Inputs:
        
    '''
    def calibrate_backbone(self, L, L_units, fr_start, alpha_start, calib_coef_start, offset_start, bounds, dire, ax, dots=True):
        # extract maximum points on curves
        max_amps = self.sweep[dire, 'amplitude'].max(0)
        freq_maxes = self.sweep[dire, 'amplitude'].idxmax(0)

        freq_units = self.sweep.index.name.split('(')[1][:-1]

        freq_maxes_Hz = freq_maxes*self.unit_mags[freq_units] #convert to Hz

        fit = dbackbone_calibration(freq_maxes_Hz, max_amps, L, fr_start, alpha_start, calib_coef_start, offset_start, bounds)

        # calculate the standard error
        SE = np.sqrt(np.sum(fit.fun ** 2)) / np.sqrt(len(fit.fun) * (len(fit.fun) - 1))

        # the rest is plotting


        ax.set_xlabel('Frequency (%s)' % freq_units)
        ax.set_ylabel('Amplitude (%s)' % L_units)

        if dots:
            ax.plot(freq_maxes, max_amps, 'ro', label='Measured Curve Maxima')

        backbone_amplitudes = np.sqrt(
            dbackbone_cantilever_displacement_calibration(
            freq_maxes_Hz, L, fit.x[0], fit.x[1], fit.x[2], fit.x[3]
            )
        )

        ax.plot(freq_maxes, backbone_amplitudes, color='k', label='Duffing Backbone Fit')
        ax.set_title('Fit: SE = %.3g, fr = %.3g, alpha = %.3g, calib_coef = %.3g, offset = %.3g' % (SE, fit.x[0], fit.x[1], fit.x[2], fit.x[3]))

        self.sweep[dire, 'amplitude'].plot(ax=ax)
        ax.plot(freq_maxes, backbone_amplitudes, color='k')




    def plot_duffing_backbone_fit(self, m_eff, freq_n_guess, alpha_guess,  dir, ax, dots=True, title = False, legend = True, legend_loc = False):
        max_amp = self.sweep[dir, 'amplitude'].max(0)
        freq_max = self.sweep[dir, 'amplitude'].idxmax(0)

        fit = fit_duffing_backbone(max_amp, freq_max, m_eff, freq_n_guess, alpha_guess)

        SE = np.sqrt(np.sum(fit.fun ** 2)) / np.sqrt(len(fit.fun) * (len(fit.fun) - 1))
        print('Fit omega_n:', fit.x[0])
        print('Fit Duffing Nonlinearity:', fit.x[1])


        freq_units = self.sweep.index.name.split('(')[1][:-1]


        ax.set_xlabel('Frequency (%s)' % freq_units)
        ax.set_ylabel('Amplitude (V)')

        if dots:
            ax.plot(freq_max, max_amp, 'ro', label='Measured Curve Maxima')

        backbone_omega_maxes = duffing_backbone(max_amp, fit.x[0], fit.x[1], m_eff)
        ax.plot(backbone_omega_maxes, max_amp, color='k', label='Duffing Backbone Fit')
        ax.set_title('Fit SE: %.6g' %SE)

        self.sweep[dir, 'amplitude'].plot(ax=ax, legend=False)
        ax.plot(backbone_omega_maxes, max_amp, color='k', label='Duffing Backbone Fit')


        '''max_displacements, omega_maxes = self.find_maxes(dir)
        #max_displacements = np.array([max(trace.amp) for trace in self.traces if trace.dir == dir])
        #omega_maxes = np.array([trace.freqs[np.argmax(trace.amp)] for trace in self.traces if trace.dir == dir])

        fit = fit_duffing_backbone(max_displacements, omega_maxes, m_eff, omega_n_guess, 0.001)
        #fit = self.fit_duffing_backbone(m_eff, omega_n_guess, dir)
        SE = np.sqrt(np.sum(fit.fun**2))/np.sqrt(len(fit.fun)*(len(fit.fun)-1))
        print('Fit omega_n:', fit.x[0])
        print('Fit Duffing Nonlinearity:', fit.x[1])
        #u_sq_maxes = np.array([max(trace.amp) for trace in self.traces if trace.dir == dir]) ** 2
        #omega_maxes = np.array([trace.freqs[np.argmax(trace.amp)] for trace in self.traces if trace.dir == dir])
        backbone_omega_maxes = duffing_backbone(max_displacements, fit.x[0], fit.x[1], m_eff)

        if not plotter:
            show = True
            plotter = plt
            plotter.xlabel('Frequency (kHz)')  # ToDo, get this from files
            plotter.ylabel('Amplitude (mV)')
            if title:
                plotter.title(title)
            else:
                plotter.title('Power Sweep with Duffing Backbone Fit. SE: %.3g mV'%SE)
        else:
            show = False
            plotter.set_xlabel('Frequency (kHz)')  # ToDo, get this from files
            plotter.set_ylabel('Amplitude (mV)')
            if title:
                plotter.set_title(title + ' SE: %.3g mV'%SE)
            else:
                plotter.set_title('Power Sweep with Duffing Backbone Fit, SE: %.3g mV'%SE)
        for trace in self.traces:
            if trace.dir == dir:
                plotter.plot(trace.freqs, trace.amp, label=trace.power)

        plotter.plot(backbone_omega_maxes, max_displacements, color='k', label='Duffing Backbone Fit')
        if dots:
            plotter.plot(omega_maxes, max_displacements, 'ro', label='Measured Curve Maxima')
        #plotter.xlabel('Frequency (kHz)') #ToDo, get this from files
        #plotter.ylabel('Amplitude (mV)')
        if legend_loc and legend:
        #if legend_scale:
            #plotter.legend(prop = {'size', legend_scale})
            plotter.legend(loc = legend_loc)
            #plotter.legend(markerscale = legend_scale)
            pass
        elif legend:
            plotter.legend()
        if show:
            #plt.tight_layout()
            plt.show()
        print('SE: '+str(SE) + 'mV')
        return fit
        '''

    '''
    This function 
    '''
    def plot_duffing_backbone_extrapolated_fit(self, m_eff, freq_n_guess, alpha_guess,  dir, ax, dots = True, title = False, legend = True, legend_loc = False):
        max_amp = self.sweep[dir, 'amplitude'].max(0)
        freq_max = self.sweep[dir, 'amplitude'].idxmax(0)

        fit = fit_duffing_backbone(max_amp, freq_max, m_eff, freq_n_guess, alpha_guess)

        SE = np.sqrt(np.sum(fit.fun ** 2)) / np.sqrt(len(fit.fun) * (len(fit.fun) - 1))
        print('Fit omega_n:', fit.x[0])
        print('Fit Duffing Nonlinearity:', fit.x[1])

        freq_units = self.sweep.index.name.split('(')[1][:-1]

        ax.set_xlabel('Frequency (%s)' % freq_units)
        ax.set_ylabel('Amplitude (V)')

        if dots:
            ax.plot(freq_max, max_amp, 'ro', label='Measured Curve Maxima')

        # find best fit, then

        backbone_omega_maxes = duffing_backbone(max_amp, fit.x[0], fit.x[1], m_eff)
        ax.plot(backbone_omega_maxes, max_amp, color='k', label='Duffing Backbone Fit')
        ax.set_title('Fit SE: %.6g' % SE)

        self.sweep[dir, 'amplitude'].plot(ax=ax, legend=False)
        ax.plot(backbone_omega_maxes, max_amp, color='k', label='Duffing Backbone Fit')

    def find_maxes(self, dir):
        max_displacements = np.array([max(trace.amp) for trace in self.traces if trace.dir == dir])
        omega_maxes = np.array([trace.freqs[np.argmax(trace.amp)] for trace in self.traces if trace.dir == dir])
        return max_displacements, omega_maxes

    def _find_fits(self, m_eff, alpha, f_n_guess):
        amp_traces = self.sweep.loc(axis=1)[:, 'amplitude']

        amp_maxes = amp_traces.max(0)
        max_freq = amp_traces.idxmax(0)

        # make new dataframe to store results
        direction, measurement, drive = max_freq.index.levels
        fit_params = ['SE', 'f_n', 'alpha_d_meff']
        #ToDo: extract drive units from max_freq.index
        levels = pd.MultiIndex.from_product([direction, fit_params], names=['Direction', 'fit'])
        # index = pd.MultiIndex([drive)
        
        # Process drive array into a sorted list of string drives that pandas 
        # will not alter them when put into an index
        fdr = [float(d) for d in drive]
        fdr.sort(reverse=True)
        
        #remove the seven lowest drives because it doesn't make sense to fit to fewer drives that.
        fdr = fdr[:-7]

        index = pd.Index(data=fdr, name='Maximum Drive (mV)')
        fits = pd.DataFrame(index=index, columns=levels)


        for dire in ('up', 'down'):
            for i, drive in enumerate(fdr):
                # if i>len(fdr)-5:

                # d = str(drive)+'mV'
                # Float integers are represented with a '.0', but this is dropped in pandas data frame keys. 
                    
                mamps = amp_maxes.loc(0)[dire]
                mfreq = max_freq.loc(0)[dire]
                fit = fit_duffing_backbone_fn(mamps[i:], mfreq[i:], alpha/m_eff, f_n_guess)
                SE = np.sqrt(np.sum(fit.fun ** 2)) / len(fit.fun)
                fits[dire, 'SE'].loc(0)[drive] = float(SE)
                fits[dire, 'f_n'].loc(0)[drive] = float(fit.x[0])
                fits[dire, 'alpha_d_meff'].loc(0)[drive] = float(fit.x[1])

        self.fits = fits

    def find_best_fit(self, m_eff, alpha, f_n_guess, dire):
        #if not self.fits: #if fits have not already been calculated, calculate them.
        self._find_fits(m_eff, alpha, f_n_guess)
        SEs = self.fits[dire, 'SE']
        SEs = pd.to_numeric(SEs)
        drive_max_opt = SEs[dire].idxmin()
        print(drive_max_opt)
        f_n = self.fits[dire, 'f_n'].loc(0)[drive_max_opt]
        return SEs


    '''    
    def lorFitTr(self, i, plot = False):
        tr = self.traces[i]
        fit = fit_lorentzian(tr.freqs, tr.amp)
        if plot:
            plt.plot(tr.freqs,tr.amp, label = 'Measured Resonance Curve')
            plt.plot(tr.freqs, lorentz(tr.freqs, *fit.x), label = 'lorentzian fit')
            plt.legend()
            plt.show()
        return fit
    
    def dufFitTr(self, i, plot = False):
        tr = self.traces[i]
        fit = fit_duffing(tr.freqs, tr.amp, tr.dir)
        if plot:
            plt.plot(tr.freqs,tr.amp, label = 'Measured Resonance Curve')
            dc = duffing_curve(tr.freqs, *fit.x, tr.dir)
            plt.plot(tr.freqs, dc, label = 'Duffing fit')
            #dc = np.sqrt(dc)
            #f = tr.freqs[np.logical_not(np.isnan(dc))]
            #c = dc[np.logical_not(np.isnan(dc))]
            #plt.plot(f, c, label = 'Duffing fit')
            plt.title(tr.power + ' ' + tr.dir + ' scan')
            plt.xlabel('Frequency khz')
            plt.ylabel('Voltage (mV)')
            plt.legend()
            plt.show()
        return fit
        '''
        
        
